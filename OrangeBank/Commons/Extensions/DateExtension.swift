//
//  DateExtension.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 26/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation

extension Date {
    // Formating date to string with a default parameter for format which can be change when calling the func
    func formatDateToString(format: String = "EEEE d MMMM yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "es_ES")
        return dateFormatter.string(from: self).localizedCapitalized
    }
}
