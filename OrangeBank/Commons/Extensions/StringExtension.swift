//
//  StringExtension.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 25/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation

extension String {
    // Formating string to date with a default parameter for format which can be change when calling the func
    func formatStringToDate(format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
}
