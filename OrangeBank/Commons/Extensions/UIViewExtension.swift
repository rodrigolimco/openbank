//
//  UIViewExtension.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 26/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T? {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as? T ?? nil
    }
    
    func addCellsShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.25
        self.layer.shadowRadius = 2.0
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
    }
}
