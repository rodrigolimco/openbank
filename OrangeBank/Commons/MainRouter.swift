//
//  MainRouter.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation
import UIKit

protocol MainRouterProtocol {
    func navigateToTransactionsScreen()
    func showLoading(show: Bool)
    func showAlert(message: String)
}

class MainRouter {
    // Project's main router; this class is responsible for navigating through the project's
    // different screens. It also shows the loading view when needed.
    
    let window: UIWindow
    private var loadingView: LoadingView = LoadingView.fromNib()!
    
    init(mainWindow: UIWindow) {
        self.window = mainWindow
    }
    
    var rootViewController: UIViewController {
        guard let rootViewController = window.rootViewController else {
            fatalError("There is no rootViewController installed on the window")
        }
        return rootViewController
    }
}

extension MainRouter: MainRouterProtocol {
    func navigateToTransactionsScreen() {
        let transactionsVC = TransactionsRouter.createModule(mainRouter: self)
        let rootViewController = UINavigationController(rootViewController: transactionsVC)
        window.rootViewController = rootViewController
    }
    
    func showLoading(show: Bool) {
        switch show {
        case true: loadingView.showIn(view: rootViewController.view)
        case false: loadingView.stopAnimating()
        }
    }
    
    func showAlert(message: String) {
        // TODO: Show alert view to notify user when something went wrong
    }
}
