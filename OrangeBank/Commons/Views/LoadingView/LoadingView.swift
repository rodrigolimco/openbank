//
//  LoadingView.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 26/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    @IBOutlet weak var ivLoadingView: UIImageView!
    
    var continueAnimating = false
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    func showIn(view: UIView) {
        if continueAnimating { // Already presented
            return
        }
        
        self.frame = view.bounds
        view.addSubview(self)
        _startAnimating()
    }
    
    private func _startAnimating() {
        continueAnimating = true
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveLinear, animations: {
            self.ivLoadingView.transform = self.ivLoadingView.transform.rotated(by: .pi)
        }, completion: { (_) in
            if let _ = self.superview, self.continueAnimating {
                self._startAnimating()
            }
        })
    }
    
    private func _stopAnimation() {
        continueAnimating = false
        self.ivLoadingView.transform = .identity
        ivLoadingView.layer.removeAllAnimations()
    }
    
    func stopAnimating() {
        _stopAnimation()
        removeFromSuperview()
    }
    
    func loadingSuccess() {
        _stopAnimation()
    }
}
