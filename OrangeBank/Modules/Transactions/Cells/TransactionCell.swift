//
//  TransactionCell.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {

    @IBOutlet weak var vwShadowView: UIView!
    @IBOutlet weak var vwContainerView: UIView!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    @IBOutlet weak var lbFee: UILabel!
    @IBOutlet weak var lbTotal: UILabel!
    
    var transaction: TransactionModel? {
        didSet {
            lbDate.text = transaction?.dateFormatted
            lbDescription.text = transaction?.description
            lbAmount.text = transaction?.amount
            lbFee.text = transaction?.fee
            lbTotal.text = transaction?.total
            [lbAmount, lbTotal].forEach { (view) in
                view?.textColor = transaction?.isIncome ?? false ? UIColor(named: "color_green") : UIColor(named: "color_red")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        vwShadowView.addCellsShadow()
        vwShadowView.layer.cornerRadius = 10
        vwContainerView.layer.cornerRadius = 10
        vwContainerView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        let labels = self.subviews.filter({$0 is UILabel}) as? [UILabel]
        labels?.forEach { (view) in
            view.text = nil
        }
    }
}
