//
//  TransactionViewModel.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 25/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation

// Model used to fill table view's cells with info
struct TransactionModel: Codable {
    let description, id, amount, fee, total: String
    let dateFormatted: String // This date is displayed as the transaction's date
    let date: Date // This date is used to compare two transactions that may have the same ID and discard the oldest one
    let hasValidTransactionDate, isIncome: Bool // hasValidTransactionDate used to discard wrong date format transactions; isIncome is used to set label's amount color
    
    
    init(_ transaction: TransactionResponse) {
        self.description = transaction.description == nil || transaction.description == "" ? "No hay detalle 😕" : transaction.description ?? "-"
        self.id = String(transaction.id ?? 0)
        self.amount = String(format: "%.2f", transaction.amount ?? 0)
        self.fee = String(format: "%.2f", transaction.fee ?? 0)
        self.total = String(format: "%.2f", (transaction.amount ?? 0) + (transaction.fee ?? 0))
        self.date = transaction.date?.formatStringToDate() ?? Date()
        self.dateFormatted = transaction.date?.formatStringToDate()?.formatDateToString() ?? ""
        self.hasValidTransactionDate = self.dateFormatted != "" ? true : false
        self.isIncome = transaction.amount ?? 1 > 0 ? true : false
    }
}
