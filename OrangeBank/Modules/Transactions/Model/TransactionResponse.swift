//
//  TransactionModel.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation

// Model used to parse incoming transactions data
// from API; all values are optional so any changes made
// to the API model shouldn't stop the app from showing data
struct TransactionResponse: Codable {
    let date, description: String?
    let amount, fee: Double?
    let id: Int?
}
