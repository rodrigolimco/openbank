//
//  TransactionsDataSource.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation
import Alamofire

final class TransactionsDataSource {
    
}

// MARK: - TransactionsDataSourceProtocol
extension TransactionsDataSource: TransactionsDataSourceProtocol {
    func requestTransactions(completion: @escaping (Result<Data>) -> Void) {
        let url: String = "https://api.myjson.com/bins/1a30k8" // TODO: Constants file with URL as project grows
        Alamofire.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                completion(.success(data))
                
            case .failure(let error):
                completion(.failure(error))
                print(error.localizedDescription)
            }
        }
    }
}
