//
//  TransactionsInteractor.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation

final class TransactionsInteractor {

    weak var presenter: TransactionsPresenterProtocol?
    private(set) var dataSource: TransactionsDataSourceProtocol
    
    init(dataSource: TransactionsDataSourceProtocol) {
        self.dataSource = dataSource
    }
}

// MARK: - TransactionsInteractorProtocol
extension TransactionsInteractor: TransactionsInteractorProtocol {
    func retrieveTransactionsData() {
        dataSource.requestTransactions { result in
            switch result {
            case .success(let data):
                do {
                    let transactionsResponse = try JSONDecoder().decode([TransactionResponse].self, from: data)
                    self.presenter?.onTransactionsRetrieved(transactions: transactionsResponse)
                } catch {
                    // TODO: Show parse error with an alert
                    print(error.localizedDescription)
                }
            case .failure(let error):
                // TODO: Show service error with an alert
                print(error.localizedDescription)
            }
        }
    }
}
