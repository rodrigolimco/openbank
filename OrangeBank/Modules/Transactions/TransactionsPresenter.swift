//
//  TransactionsPresenter.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation

final class TransactionsPresenter  {
    private(set) weak var view: TransactionsViewProtocol?
    private(set) var interactor: TransactionsInteractorProtocol
    private(set) var router: TransactionsRouterProtocol
    internal var userTransactions: [TransactionModel] = []
    
    init(view: TransactionsViewProtocol, interactor: TransactionsInteractorProtocol, router: TransactionsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

// MARK: - TransactionsPresenterProtocol
extension TransactionsPresenter: TransactionsPresenterProtocol {
    func retrieveTransactions() {
        interactor.retrieveTransactionsData()
    }
    
    func onTransactionsRetrieved(transactions: [TransactionResponse]) {
        userTransactions.removeAll()
        // Removing from response array all transactions with invalid date format
        userTransactions = transactions.compactMap(TransactionModel.init)
            .filter({ $0.hasValidTransactionDate == true })
        
        self.userTransactions = _removeDuplicates(from: userTransactions)
        view?.reloadData()
    }
    
    func showLoading(show: Bool) {
        router.showLoading(show: show)
    }
    
    // Function to remove duplicate transactions from response
    private func _removeDuplicates(from array: [TransactionModel]) -> [TransactionModel] {
        // Array with duplicated transactions
        let duplicateTransactions = array.filter { transaction in
            array.filter({ $0.id == transaction.id}).count != 1
        }
        
        // Array with most recent dates from duplicated transactions
        let uniqueDuplicateds = duplicateTransactions.filter { transaction in
            duplicateTransactions.filter({ $0.date > transaction.date }).count == 1 }
        
        // Original array without all of the duplicated transactions
        var filteredTransactions = array.filter { transaction in
            array.filter({ $0.id == transaction.id}).count == 1
        }
        // Appending filtered duplicated transactions to original array
        filteredTransactions.append(contentsOf: uniqueDuplicateds)
        // Sorting final array by date
        filteredTransactions.sort { (transaction1, transaction2) in
            transaction1.date > transaction2.date
        }
        
        return filteredTransactions
    }
}
