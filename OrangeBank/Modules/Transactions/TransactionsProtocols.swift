//
//  TransactionsProtocols.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import Foundation
import Alamofire

// MARK: Wireframe
protocol TransactionsRouterProtocol: AnyObject {
    func showLoading(show: Bool)
}

// MARK: Data Source
protocol TransactionsDataSourceProtocol: AnyObject {
    func requestTransactions(completion: @escaping (Result<Data>) -> Void) // Use of Swift5's custom Result type
}

// MARK: Interactor
protocol TransactionsInteractorProtocol: AnyObject {
    var presenter: TransactionsPresenterProtocol? { get }
    var dataSource: TransactionsDataSourceProtocol { get }
    
    func retrieveTransactionsData()
}

// MARK: Presenter
protocol TransactionsPresenterProtocol: AnyObject {
    var interactor: TransactionsInteractorProtocol { get }
    var router: TransactionsRouterProtocol { get }
    var view: TransactionsViewProtocol? { get }
    var userTransactions: [TransactionModel] { get }
    
    func retrieveTransactions()
    func onTransactionsRetrieved(transactions: [TransactionResponse])
    func showLoading(show: Bool)
}

// MARK: View
protocol TransactionsViewProtocol: AnyObject {
    var presenter: TransactionsPresenterProtocol! { get }
    
    func reloadData()
}
