//
//  TransactionsRouter.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import UIKit

final class TransactionsRouter {
   
    var mainRouter: MainRouterProtocol?
    
    init(mainRouter: MainRouterProtocol) {
        self.mainRouter = mainRouter
    }
    
    static func createModule(mainRouter: MainRouterProtocol) -> UIViewController {
        let router = TransactionsRouter(mainRouter: mainRouter)
        let view = TransactionsViewController()
        let dataSource = TransactionsDataSource()
        let interactor = TransactionsInteractor(dataSource: dataSource)
        let presenter = TransactionsPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        return view
    }
}

// MARK: - TransactionsRouterProtocol
extension TransactionsRouter: TransactionsRouterProtocol {
    func showLoading(show: Bool) {
        mainRouter?.showLoading(show: show)
    }
}
