//
//  TransactionsViewController.swift
//  OrangeBank
//
//  Created by Rodrigo Limpias Cossio on 24/01/2020.
//  Copyright © 2020 Rodrigo Limpias Cossio. All rights reserved.
//

import UIKit

final class TransactionsViewController: UIViewController {

    var presenter: TransactionsPresenterProtocol!
    private let refreshControl = UIRefreshControl()

    @IBOutlet weak var tvTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _configViews()
        _configRefreshControl()
        _retrieveUserTransactions() // Requesting data when view did load
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    private func _configViews() {
        tvTableView.delegate = self
        tvTableView.dataSource = self
        tvTableView.separatorStyle = .none
        tvTableView.refreshControl = refreshControl
        tvTableView.register(UINib(nibName: String(describing: TransactionCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TransactionCell.self))
    }
    
    private func _configRefreshControl() {
        refreshControl.tintColor = UIColor(named: "color_orange")
        refreshControl.addTarget(self, action: #selector(_pulledToRefresh), for: .valueChanged)
    }
    
    private func _retrieveUserTransactions() {
        presenter.showLoading(show: true)
        presenter.retrieveTransactions()
    }
    
    @objc private func _pulledToRefresh() {
        presenter.retrieveTransactions()
    }
}

// MARK: - TransactionsViewProtocol
extension TransactionsViewController: TransactionsViewProtocol {
    func reloadData() {
        tvTableView.reloadData()
        if refreshControl.isRefreshing {
            // Setting a delay for endRefreshing as API response is fast,
            // giving the feeling of a bad implementation
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.refreshControl.endRefreshing()
            }
        }
        presenter.showLoading(show: false)
    }
}

extension TransactionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.userTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TransactionCell.self), for: indexPath) as? TransactionCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.transaction = presenter.userTransactions[indexPath.row]
        return cell
    }
}
