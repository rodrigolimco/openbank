# OpenBank

## Description
- Code test for **OpenBank Spain** made by **Rodrigo Limpias**.  

- Mobile application for iOS platform which allows to view a list of the user's bank account transactions.

- The project implements **VIPER** architecture.
<br>
<br>

## Technology
- Xcode 11.3

- Swift 5
<br>
<br>

## CocoaPods
- **Alamofire**, CocoaPods library used to handle network connections.